const express= require("express");
const https = require("https");
const bodyParser= require("body-parser");
const app= express();

app.use(bodyParser.urlencoded({extended:true}));


app.get("/",function(req,res){
    res.sendFile(__dirname+ "/index.html");
});

app.post("/",function(req,res){
    //console.log(req.body.cityname);
const query= req.body.cityname;
const apiKey= "fa9d3c4b3e78a3d59d1ba7cb3c66e5c4";
const url= "https://api.openweathermap.org/data/2.5/weather?q=" + query + "&appid=" + apiKey+"&units=metric";
//const url= "https://api.openweathermap.org/data/2.5/weather?q=London&appid=fa9d3c4b3e78a3d59d1ba7cb3c66e5c4&units=metric";
https.get(url,function(response){
    console.log(response.statusCode);

    response.on("data",function(data){
        const weatherData=JSON.parse(data);
        const temp = weatherData.main.temp;
        const weatherDescription= weatherData.weather[0].description;
        const icon= weatherData.weather[0].icon;
        const imageURL="https://openweathermap.org/img/wn/"+icon+"@2x.png";
        //const imageURL="https://openweathermap.org/img/wn/10d@2x.png";
        res.write("<h1> the temperature in " +req.body.cityname+" is: "+ temp+ " degree celsius.</h1>")
        res.write("<h3>the weather description is:" + weatherDescription+"</h3>");
        res.write("<img src="+imageURL+">")
        res.send();
    })


});
    
}); 


 




app.listen(3000,function(){
    console.log("Server started on port 3000");
});